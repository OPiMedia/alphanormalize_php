.SUFFIXES:
.SUFFIXES:	.dvi .pdf .ps .tex

VERSION = 03_00_02

###########
# Options #
###########
CHECKTXT = checkTxt.pl

DOXYGEN = doxygen

PHPDOC = phpdoc.bat

PHPstripDEBUG = PHPstripDEBUG.py


7Z     = 7z
CD     = cd
CP     = cp
MKDIR  = mkdir -p
MV     = mv
RM     = rm -f
SHELL  = sh



###
# #
###
.PHONY:	all checkTxt dists docs docs-doxygen docs-phpdoc stripped

all:	docs stripped checkTxt

checkTxt:
	$(CHECKTXT) +lisible -put +questions -E^_dist/ -E^_img/alphanormalize_php_ * */*

dists:	docs-doxygen stripped
	$(CP) -t _tmp/stripped/alphanormalize_php README.rst
	$(MKDIR) _tmp/stripped/alphanormalize_php/COPYING
	$(CP) -t _tmp/stripped/alphanormalize_php/COPYING alphanormalize_php/COPYING/*

	$(CP) -t _tmp/alphanormalize_php README.rst
	$(MKDIR) _tmp/alphanormalize_php/COPYING
	$(CP) -t _tmp/alphanormalize_php/COPYING alphanormalize_php/COPYING/*

	$(MV) _tmp/stripped/alphanormalize_php _tmp/stripped/alphanormalize_php_stripped
	$(MV) _tmp/docs _tmp/alphanormalize_php_html

	$(CD) _tmp/stripped; $(7Z) a -ttar ../alphanormalize_php_stripped.tar alphanormalize_php_stripped
	$(MV) _tmp/stripped/alphanormalize_php_stripped _tmp/stripped/alphanormalize_php

	$(CD) _tmp/; $(7Z) a -ttar alphanormalize_php.tar alphanormalize_php

	$(CD) _tmp/; $(7Z) a -ttar alphanormalize_php_html.tar alphanormalize_php_html
	$(MV) _tmp/alphanormalize_php_html _tmp/docs

	$(CD) _tmp; $(7Z) a -tgzip -mx=9 -mfb=258 -mpass=15 alphanormalize_php_$(VERSION)_stripped.tar.gz alphanormalize_php_stripped.tar
	$(RM) _tmp/alphanormalize_php_stripped.tar

	$(CD) _tmp; $(7Z) a -tgzip -mx=9 -mfb=258 -mpass=15 alphanormalize_php_$(VERSION).tar.gz alphanormalize_php.tar
	$(RM) _tmp/alphanormalize_php.tar

	$(CD) _tmp; $(7Z) a -tgzip -mx=9 -mfb=258 -mpass=15 alphanormalize_php_$(VERSION)_html.tar.gz alphanormalize_php_html.tar
	$(RM) _tmp/alphanormalize_php_html.tar

	$(7Z) t _tmp/alphanormalize_php_$(VERSION).tar.gz
	$(7Z) t _tmp/alphanormalize_php_$(VERSION)_stripped.tar.gz
	$(7Z) t _tmp/alphanormalize_php_$(VERSION)_html.tar.gz

docs: docs-doxygen

docs-doxygen:
	$(MKDIR) _tmp
	$(CD) alphanormalize_php; $(DOXYGEN)
	$(RM) alphanormalize_php/doxygen_sqlite3.db

docs-phpdoc:
	$(CD) alphanormalize_php; $(PHPDOC) -f *.inc,alphanormalize-test.php -t ../_tmp/docs --title=alphanormalize_php --force --validate --visibility public,protected,private --sourcecode

stripped:
	$(MKDIR) _tmp/alphanormalize_php
	$(CP) -t _tmp/alphanormalize_php/ alphanormalize_php/*.inc alphanormalize_php/alphanormalize-test.php
	$(CD) _tmp; $(PHPstripDEBUG) alphanormalize_php -r -E utf_8 -d stripped -h html -q -s



#########
# Clean #
#########
.PHONY:	clean distclean

clean:
	$(RM) checkTxt.log
	$(RM) alphanormalize_php/doxygen_sqlite3.db
	$(RM) -r _tmp

distclean:	clean
