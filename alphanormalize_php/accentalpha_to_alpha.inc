<?php // -*- coding: utf-8 -*-

/** \file accentalpha_to_alpha.inc
 * (December 31, 2013)
 *
 * \brief Characters with accent, cedilla ... or ligature.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: character with accent, cedilla ... or ligature => alphabetic character corresponding.
 *
 * For example: <code>'é'</code> => <code>'e'</code>.
 */
$ACCENTALPHA_TO_ALPHA = array(
  'À' => 'A',
  'Á' => 'A',
  'Â' => 'A',
  'Ã' => 'A',
  'Ä' => 'A',
  'Å' => 'A',
  'Æ' => 'AE',
  'Ç' => 'C',
  'È' => 'E',
  'É' => 'E',
  'Ê' => 'E',
  'Ë' => 'E',
  'Ì' => 'I',
  'Í' => 'I',
  'Î' => 'I',
  'Ï' => 'I',
  'Ñ' => 'N',
  'Ò' => 'O',
  'Ó' => 'O',
  'Ô' => 'O',
  'Õ' => 'O',
  'Ö' => 'O',
  'Ø' => 'O',
  'Ù' => 'U',
  'Ú' => 'U',
  'Û' => 'U',
  'Ü' => 'U',
  'Ý' => 'Y',
  'ß' => 'sz',
  'à' => 'a',
  'á' => 'a',
  'â' => 'a',
  'ã' => 'a',
  'ä' => 'a',
  'å' => 'a',
  'æ' => 'ae',
  'ç' => 'c',
  'è' => 'e',
  'é' => 'e',
  'ê' => 'e',
  'ë' => 'e',
  'ì' => 'i',
  'í' => 'i',
  'î' => 'i',
  'ï' => 'i',
  'ñ' => 'n',
  'ò' => 'o',
  'ó' => 'o',
  'ô' => 'o',
  'õ' => 'o',
  'ö' => 'o',
  'ø' => 'o',
  'ù' => 'u',
  'ú' => 'u',
  'û' => 'u',
  'ü' => 'u',
  'ý' => 'y',
  'ÿ' => 'y',
  'Œ' => 'OE',
  'œ' => 'oe',
  'Š' => 'S',
  'š' => 's',
  'Ÿ' => 'Y'
);


return true;

?>