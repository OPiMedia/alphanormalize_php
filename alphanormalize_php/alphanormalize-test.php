<?php // -*- coding: utf-8 -*-

/** \file alphanormalize-test.php
 * (June 17, 2020)
 *
 * \brief PHP/HTML page to test alphanormalize_php.
 */

ini_set('display_errors', 'stdout');
ini_set('display_startup_errors', 1);
ini_set('html_errors', 1);

error_reporting(-1);

assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_WARNING, true);
assert_options(ASSERT_BAIL, true);

mb_internal_encoding('UTF-8');

require_once 'alphanormalize.inc';

require_once 'html_entities_entitydefs.inc';
require_once 'html_entities_name2codepoint.inc';
require_once 'html_entities_codepoint2name.inc';

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Reply-To" content="olivier.pirson.opi@gmail.com" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>alphanormalize_php &ndash; tests</title>

    <meta name="copyright" content="&copy; Olivier Pirson" />
    <meta name="author" content="Olivier Pirson" />
    <meta name="date-creation-yyyymmdd" content="20101005" />
    <meta name="date-revision-yyyymmdd" content="20200617" />
    <meta name="description" content="Simple functions to remove accents and  replace non-alphanumeric characters." />
    <meta name="keywords" content="PHP, character, accents, Greek" />

    <style type="text/css">
h1 {
    margin: 1ex 0 0;
    text-align:center;
}

table { border-collapse: collapse; }

tr { vertical-align: top; }

tr.sep { border-top:solid 3px silver; }

th {
    font-family: Dina, monospace;
    font-weight: normal;
    text-align: left;
}

th,
td {
    border: solid 1px silver;
    white-space: nowrap;
}
td.sep { background-color: silver; }

hr {
    border-style: none;
    border-top: dotted 1px silver;
}

a.mail:link {
    background-color: inherit;
    color: green;
}
a.mail:visited {
    background-color: inherit;
    color: olive;
}

div { margin-top: 3ex; }

pre { margin: 0; }

.center { text-align: center; }

.smallskip { margin-top: 1ex; }
.bigskip    { margin-top: 3ex; }
.bigbigskip { margin-top: 6ex; }
.bigbigbigskip { margin-top: 9ex; }

.green { color: green; }
.red { color: red; }

.surname { font-variant: small-caps; }
    </style>
  </head>
  <body>
    <h1><a rel="nofollow" href="https://bitbucket.org/OPiMedia/alphanormalize_php" style="text-decoration:none"><img src="https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/12/1411058763-4-alphanormalize_php-logo_avatar.png" width="48" height="48" alt="" style="border:none; vertical-align:top" />&nbsp;</a><a href="https://bitbucket.org/OPiMedia/alphanormalize_php"><tt>alphanormalize_php</tt></a> &ndash; tests</h1>
    <div class="smallskip center">
      <a rel="nofollow" lang="fr" href="http://www.opimedia.be/" style="text-decoration:none"><img src="http://www.opimedia.be/_png/OPi.png" width="41" height="34" alt="" style="border:none; vertical-align:middle" />&nbsp;</a><a lang="fr" href="http://www.opimedia.be/">Olivier <span class="surname">Pirson</span></a>
      &ndash;
      <a class="mail" href="mailto:olivier.pirson.opi@gmail.com?subject=[alphanormalize_php]"><tt>olivier.pirson.opi@gmail.com</tt></a>
      &ndash;
      v.<?php echo preg_replace('/---/', '&ndash;', \Alphanormalize\version()); ?>
    </div>
    <div class="smallskip center">
      <?php echo 'PHP version ', PHP_VERSION; ?>
    </div>
    <ul>
      <li>Sources on Bitbucket: <a href="https://bitbucket.org/OPiMedia/alphanormalize_php"><tt>https://bitbucket.org/OPiMedia/alphanormalize_php</tt></a></li>
      <li><a href="http://www.opimedia.be/DS/webdev/PHP/alphanormalize-php/docs/">Online HTML documentation</a></li>
      <li><a href="http://www.opimedia.be/DS/webdev/PHP/alphanormalize-php/alphanormalize-test.php">Online simple test page</a></li>
    </ul>

    <div class="bigskip">
<?php

$s = '<pre>Il était une fois&hellip; des <em>caractères</em> "bizarroïdes" (avec accent, cédille et compagnie, ou α, β, etc.)
accompagnés ou pas de formatage <em>(X)HTML</em>.
Je cherchais à m&rsquo;en débarrasser&nbsp;!
Ceci est ma solution.</pre>';

?>
      <table>
        <tr>
          <th>$s</th>
          <td class="sep"></td>
          <td><?php echo $s; ?></td>
        </tr>

        <tr class="sep">
          <th>mb_str_accentalpha_to_alpha($s)</th>
          <td class="sep"></td>
          <td><?php echo \Alphanormalize\mb_str_accentalpha_to_alpha($s); ?></td>
        </tr>
        <tr>
          <th>mb_str_greek_to_alpha($s)</th>
          <td class="sep"></td>
          <td><?php echo \Alphanormalize\mb_str_greek_to_alpha($s); ?></td>
        </tr>

        <tr class="sep">
          <th>htmlspecialchars($s)</th>
          <td class="sep"></td>
          <td><?php echo htmlspecialchars($s); ?></td>
        </tr>
        <tr>
          <th><strong>mb_str_alphanormalize($s)</strong></th>
          <td class="sep"></td>
          <td><?php echo \Alphanormalize\mb_str_alphanormalize($s); ?></td>
        </tr>
        <tr>
          <th><strong>mb_str_alphanormalize($s, true)</strong></th>
          <td class="sep"></td>
          <td><?php echo \Alphanormalize\mb_str_alphanormalize($s, true); ?></td>
        </tr>
      </table>
    </div>

    <div>
      <tt>$ACCENTALPHA_TO_ALPHA[]</tt>:
      <table>
<?php

require 'accentalpha_to_alpha.inc';

echo '        <tr>
        <th>$c</th>
';

foreach ($ACCENTALPHA_TO_ALPHA as $c=>$v) {
  if (in_array($c, array('À', 'ß', 'à', 'Œ'), true)) {
    echo '<td class="sep"></td>
';
  }

  echo '<td>', $c, '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>$ACCENTALPHA_TO_ALPHA[$c]</th>
';

foreach ($ACCENTALPHA_TO_ALPHA as $c=>$v) {
  if (in_array($c, array('À', 'ß', 'à', 'Œ'), true)) {
    echo '<td class="sep"></td>
';
  }

  echo '<td>', $v, '</td>
';
}

echo '        </tr>
';

?>
      </table>
    </div>

    <div>
      <tt>$GREEK_TO_ALPHA[]</tt>
      (Adopts the standard <acronym>ONU</acronym>/<acronym>ELOT</acronym>:
       see. <i>Memento <a href="http://www.opimedia.be/DS/mementos/grecs.htm">grecs</a></i>):
      <table>
<?php

require 'greek_to_alpha.inc';

echo '        <tr>
        <th>$c</th>
';

foreach ($GREEK_TO_ALPHA as $c=>$v) {
  if (in_array($c, array('Α', 'α', 'ς', 'σ', 'ϑ'), true)) {
    echo '<td class="sep"></td>
';
  }

  echo '<td>', $c, '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>$GREEK_TO_ALPHA[$c]</th>
';

foreach ($GREEK_TO_ALPHA as $c=>$v) {
  if (in_array($c, array('Α', 'α', 'ς', 'σ', 'ϑ'), true)) {
    echo '<td class="sep"></td>
';
  }

  echo '<td>', $v, '</td>
';
}

echo '        </tr>
';

?>
      </table>
    </div>

    <div class="bigbigbigskip">
      Associative table returned by the PHP function
      <tt>get_html_translation_table(HTML_SPECIALCHARS)</tt>:
      <table>
<?php

echo '        <tr>
        <th>htmlspecialchars($c)</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_SPECIALCHARS) as $c=>$v) {
  echo '<td>', htmlspecialchars($c), '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>htmlspecialchars(get_html_translation_table(HTML_SPECIALCHARS)[$c])</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_SPECIALCHARS) as $c=>$v) {
  echo '<td>', htmlspecialchars($v), '</td>
';
}

echo '        </tr>
        <tr>
        <th>mb_str_alphanormalize($c)</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_SPECIALCHARS) as $c=>$v) {
  echo '<td>', \Alphanormalize\mb_str_alphanormalize($c), '</td>
';
}

echo '        </tr>
';

?>
      </table>
    </div>

    <div>
      Associative table returned by the PHP function
      <tt>get_html_translation_table(HTML_ENTITIES)</tt>:
      <table>
<?php

echo '        <tr>
        <th>htmlspecialchars(utf8_encode($c))</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_ENTITIES) as $c=>$v) {
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_encode($c);
  }
  echo '<td', (in_array($c, $HTML_ENTITIES_ENTITYDEFS, true)
               ? '>'
               : ' class="red">'), htmlspecialchars($c), '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>htmlspecialchars(get_html_translation_table(HTML_ENTITIES)[$c])</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_ENTITIES) as $c=>$v) {
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_encode($c);
  }
  echo '<td', (in_array($c, $HTML_ENTITIES_ENTITYDEFS, true)
               ? '>'
               : ' class="red">'), htmlspecialchars($v), '</td>
';
}

echo '        </tr>
        <tr>
        <th>mb_str_alphanormalize(utf8_encode($c))</th>
        <td class="sep"></td>
';

foreach (get_html_translation_table(HTML_ENTITIES) as $c=>$v) {
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_encode($c);
  }
  echo '<td', (in_array($c, $HTML_ENTITIES_ENTITYDEFS, true)
               ? '>'
               : ' class="red">'), \Alphanormalize\mb_str_alphanormalize($c), '</td>
';
}

echo '        </tr>
';

?>
      </table>
      (<?php echo count(get_html_translation_table(HTML_ENTITIES)); ?> éléments)
    </div>

    <div class="bigbigbigskip">
      Three associative tables (copied from Python dictionary of module
      <a href="http://docs.python.org/py3k/library/html.entities.html#module-html.entities"><tt>html.entities</tt></a>):<br />
      <tt>$HTML_ENTITIES_ENTITYDEFS[]</tt>:
      <table>
<?php

$array_get_html_translation_table = get_html_translation_table(HTML_ENTITIES);

echo '        <tr>
        <th>$k</th>
        <td class="sep"></td>
';

foreach ($HTML_ENTITIES_ENTITYDEFS as $k=>$v) {
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $v = utf8_decode($v);
  }

  echo '<td', (array_key_exists($v, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), $k, '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>htmlspecialchars($HTML_ENTITIES_ENTITYDEFS[$k])</th>
        <td class="sep"></td>
';
$nb_diff = 0;

foreach ($HTML_ENTITIES_ENTITYDEFS as $k=>$v) {
  $dv = (version_compare(PHP_VERSION, '5.4.0') < 0
         ? utf8_decode($v)
         : $v);

  if (!array_key_exists($dv, $array_get_html_translation_table)) {
    $nb_diff++;
  }
  echo '<td', (array_key_exists($dv, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), htmlspecialchars($v), '</td>
';
}

echo '        </tr>
';

?>
      </table>
      (In <span class="green">green</span> the <?php echo $nb_diff; ?> items over <?php echo count($HTML_ENTITIES_ENTITYDEFS); ?>
       which are not in <tt>get_html_translation_table(HTML_ENTITIES)</tt>.
       Previous PHP 5.4, all HTML entities are not supported!)
    </div>

    <div>
      <tt>$HTML_ENTITIES_NAME2CODEPOINT[]</tt>:
      <table>
<?php

echo '        <tr>
        <th>$k</th>
        <td class="sep"></td>
';

foreach ($HTML_ENTITIES_NAME2CODEPOINT as $k=>$v) {
  $c = $HTML_ENTITIES_ENTITYDEFS[$k];
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_decode($c);
  }

  echo '<td', (array_key_exists($c, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), $k, '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>$HTML_ENTITIES_NAME2CODEPOINT[$k]</th>
        <td class="sep"></td>
';

foreach ($HTML_ENTITIES_NAME2CODEPOINT as $k=>$v) {
  $c = $HTML_ENTITIES_ENTITYDEFS[$k];
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_decode($c);
  }

  echo '<td', (array_key_exists($c, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), $v, '</td>
';
}

echo '        </tr>
';

?>
      </table>
    </div>

    <div>
      <tt>$HTML_ENTITIES_CODEPOINT2NAME[]</tt>:
      <table>
<?php

echo '        <tr>
        <th>$k</th>
        <td class="sep"></td>
';

foreach ($HTML_ENTITIES_CODEPOINT2NAME as $k=>$v) {
  if ($k === 338) {
    echo '<td class="sep"></td>
';
  }

  $c = $HTML_ENTITIES_ENTITYDEFS[$v];
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_decode($c);
  }

  echo '<td', (array_key_exists($c, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), $k, '</td>
';
}

echo '        </tr>
        <tr class="sep">
        <th>$HTML_ENTITIES_CODEPOINT2NAME[$k]</th>
        <td class="sep"></td>
';

foreach ($HTML_ENTITIES_CODEPOINT2NAME as $k=>$v) {
  if ($k === 338) {
    echo '<td class="sep"></td>
';
  }

  $c = $HTML_ENTITIES_ENTITYDEFS[$v];
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_decode($c);
  }

  echo '<td', (array_key_exists($c, $array_get_html_translation_table)
               ? '>'
               : ' class="green">'), $v, '</td>
';
}

echo '        </tr>
';

?>
      </table>
    </div>

<?php

/* Tests */
foreach (get_html_translation_table(HTML_ENTITIES) as $c=>$v) {
  if (version_compare(PHP_VERSION, '5.4.0') < 0) {
    $c = utf8_encode($c);
  }
  $v = mb_substr($v, 1, mb_strlen($v) - 2);
  assert($HTML_ENTITIES_ENTITYDEFS[$v] === $c);
}

foreach ($HTML_ENTITIES_NAME2CODEPOINT as $k=>$v) {
  assert($HTML_ENTITIES_CODEPOINT2NAME[$HTML_ENTITIES_NAME2CODEPOINT[$k]] === $k);
}

foreach ($HTML_ENTITIES_CODEPOINT2NAME as $k=>$v) {
  assert($HTML_ENTITIES_NAME2CODEPOINT[$HTML_ENTITIES_CODEPOINT2NAME[$k]] === $k);
}

?>
  </body>
</html>
