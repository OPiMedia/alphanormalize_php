<?php // -*- coding: utf-8 -*-

/** \file greek_to_alpha.inc
 * (December 31, 2013)
 *
 * \brief Greek letters.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: Greek letter => Roman alphabetic character corresponding.
 *
 * For example: character of the Greek letter epsilon <code>'ε'</code> => <code>'e'</code>.
 *
 * Adopts the standard ONU/ELOT : see http://www.opimedia.be/DS/mementos/grecs.htm .
 */
$GREEK_TO_ALPHA = array(
  'Α' => 'A',
  'Β' => 'V',
  'Γ' => 'G',
  'Δ' => 'D',
  'Ε' => 'E',
  'Ζ' => 'Z',
  'Η' => 'I',
  'Θ' => 'TH',
  'Ι' => 'I',
  'Κ' => 'K',
  'Λ' => 'L',
  'Μ' => 'M',
  'Ν' => 'N',
  'Ξ' => 'X',
  'Ο' => 'O',
  'Π' => 'P',
  'Ρ' => 'R',
  'Σ' => 'S',
  'Τ' => 'T',
  'Υ' => 'Y',
  'Φ' => 'F',
  'Χ' => 'CH',
  'Ψ' => 'PS',
  'Ω' => 'O',
  'α' => 'a',
  'β' => 'v',
  'γ' => 'g',
  'δ' => 'd',
  'ε' => 'e',
  'ζ' => 'z',
  'η' => 'i',
  'θ' => 'th',
  'ι' => 'i',
  'κ' => 'k',
  'λ' => 'l',
  'μ' => 'm',
  'ν' => 'n',
  'ξ' => 'x',
  'ο' => 'o',
  'π' => 'p',
  'ρ' => 'r',
  'ς' => 's',
  'σ' => 's',
  'τ' => 't',
  'υ' => 'y',
  'φ' => 'f',
  'χ' => 'ch',
  'ψ' => 'ps',
  'ω' => 'o',
  'ϑ' => 'th',
  'ϒ' => 'y',
  'ϖ' => 'p'
);


return true;

?>