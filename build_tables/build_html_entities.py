#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Build this PHP files (December 31, 2013):
  html_entities_entitydefs.inc
  html_entities_name2codepoint.inc
  html_entities_codepoint2name.inc

Piece of alphanormalize_php.
https://bitbucket.org/OPiMedia/alphanormalize_php

GPLv3 --- Copyright (C) 2013 Olivier Pirson
http://www.opimedia.be/
"""

import html.entities


l = []
for k in sorted(html.entities.entitydefs):
    l.append('\'' + k + '\' => \'' + html.entities.entitydefs[k] + '\'')

f = open('html_entities_entitydefs.inc', mode='w', encoding='utf_8', newline='\n')

print("""<?php // -*- coding: utf-8 -*-

/** \\file html_entities_entitydefs.inc
 *
 * \\brief Name of XHTML entities to characters.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: name of XHTML entity => corresponding character.
 *
 * For example: <code>'eacute'</code> => <code>'�'</code>.
 *
 * (This is a copy of the Python table <code>html.entities.entitydefs</code>:
 * "A dictionary mapping XHTML 1.0 entity definitions to their replacement text in ISO Latin-1."
 * http://docs.python.org/py3k/library/html.entities.html#module-html.entities .)
 */
$HTML_ENTITIES_ENTITYDEFS = array(
  """ + ',\n  '.join(l) + """
);


return true;

?>""", end='', file=f)

f.close()


l = []
for k in sorted(html.entities.name2codepoint):
    l.append('\'' + k + '\' => ' + str(html.entities.name2codepoint[k]))

f = open('html_entities_name2codepoint.inc', mode='w', encoding='utf_8', newline='\n')

print("""<?php // -*- coding: utf-8 -*-

/** \\file html_entities_name2codepoint.inc
 *
 * \\brief Name of XHTML entities to codes.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: name of XHTML entity => corresponding character code.
 *
 * For example: <code>'eacute'</code> => <code>233</code>.
 *
 * (This is a copy of the Python table <code>html.entities.name2codepoint</code>:
 * "A dictionary that maps HTML entity names to the Unicode codepoints."
 * See http://docs.python.org/py3k/library/html.entities.html#module-html.entities .)
 */
$HTML_ENTITIES_NAME2CODEPOINT = array(
  """ + ',\n  '.join(l) + """
);


return true;

?>""", end='', file=f)

f.close()


l = []
for k in sorted(html.entities.codepoint2name):
    l.append(str(k) + ' => \'' + html.entities.codepoint2name[k] + '\'')

f = open('html_entities_codepoint2name.inc', mode='w', encoding='utf_8', newline='\n')

print("""<?php // -*- coding: utf-8 -*-

/** \\file html_entities_codepoint2name.inc
 *
 * \\brief Character codes to XHTML entities.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: character code => name of the corresponding XHTML entity.
 *
 * For example : <code>233</code> => <code>'eacute'</code>.
 *
 * (This is a copy of the Python table <code>html.entities.codepoint2name</code>:
 * "A dictionary that maps Unicode codepoints to HTML entity names.".
 * See http://docs.python.org/py3k/library/html.entities.html#module-html.entities .)
 */
$HTML_ENTITIES_CODEPOINT2NAME = array(
  """ + ',\n  '.join(l) + """
);


return true;

?>""", end='', file=f)

f.close()
