#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Build this PHP files (December 31, 2013):
  accentalpha_to_alpha.inc
  greek_to_alpha.inc

Piece of alphanormalize_php.
https://bitbucket.org/OPiMedia/alphanormalize_php

GPLv3 --- Copyright (C) 2013 Olivier Pirson
http://www.opimedia.be/
"""

import html.entities
import re


l = []
for k in html.entities.entitydefs:
    r = re.match('([A-Za-z])(?:acute|caron|cedil|circ|grave|ring|slash|tilde|uml)', k)
    if r:
        c = r.group(1)
    else:
        r = re.match('([A-Za-z][A-Za-z])lig', k)
        if r:
            c = r.group(1)
        else:
            continue

    l.append('\'' + html.entities.entitydefs[k] + '\' => \'' + c + '\'')

f = open('accentalpha_to_alpha.inc', mode='w', encoding='utf_8', newline='\n')

print("""<?php // -*- coding: utf-8 -*-

/** \\file accentalpha_to_alpha.inc
 *
 * \\brief Characters with accent, cedilla ... or ligature.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: character with accent, cedilla ... or ligature => alphabetic character corresponding.
 *
 * For example: <code>'é'</code> => <code>'e'</code>.
 */
$ACCENTALPHA_TO_ALPHA = array(
  """ + ',\n  '.join(sorted(l)) + """
);


return true;

?>""", end='', file=f)

f.close()


l = []
for i in range(913, 970):
    if (i == 930) or (938 <= i <= 944):
        continue

    if i in (914, 946):
        c = 'v'
    elif i in (919, 951):
        c = 'i'
    elif i in (933, 965):
        c = 'y'
    elif i in (934, 966):
        c = 'f'
    else:
        c = html.entities.codepoint2name[i][0:(2 if i in (920, 935, 936,
                                                          952, 967, 968)
                                               else 1)]
    if i <= 937:
        c = c.upper()
    l.append('\'' + chr(i) + '\' => \'' + c + '\'')

l.append('\'' + chr(977) + '\' => \'th\'')
l.append('\'' + chr(978) + '\' => \'y\'')
l.append('\'' + chr(982) + '\' => \'p\'')

f = open('greek_to_alpha.inc', mode='w', encoding='utf_8', newline='\n')

print("""<?php // -*- coding: utf-8 -*-

/** \\file greek_to_alpha.inc
 *
 * \\brief Greek letters.
 *
 * Piece of alphanormalize_php.
 * https://bitbucket.org/OPiMedia/alphanormalize_php
 *
 * GPLv3 --- Copyright (C) 2013 Olivier Pirson
 * http://www.opimedia.be/
 */


/**
 * Associative table: Greek letter => Roman alphabetic character corresponding.
 *
 * For example: character of the Greek letter epsilon <code>'ε'</code> => <code>'e'</code>.
 *
 * Adopts the standard ONU/ELOT : see http://www.opimedia.be/DS/mementos/grecs.htm .
 */
$GREEK_TO_ALPHA = array(
  """ + ',\n  '.join(sorted(l)) + """
);


return true;

?>""", end='', file=f)

f.close()
